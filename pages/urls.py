from django.urls import path

from .views import homePageView

#code bellow

urlpatterns = [
    path('', homePageView, name='home')
]